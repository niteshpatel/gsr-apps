﻿/*
 The MIT License

 Copyright (c) 2013 Nitesh Patel http://gamestartjs.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

function Painter() {
    var self = this;

    self.onCreate = function () {
        self.paintStack = [];
        self.sorter = function (a, b) {
            return a.zIndex - b.zIndex;
        };
    };

    self.add = function (handler, zIndex) {
        self.paintStack.push({'handler': handler, 'zIndex': zIndex});
    };

    self.paint = function (c) {
        var zIndexSorted = self.paintStack.slice().sort(self.sorter);
        for (var i = 0; i < zIndexSorted.length; i++) {
            zIndexSorted[i].handler(c);
        }
        self.paintStack = [];
    };

    self.onCreate();
}

function Subtasks() {
    var self = this;

    self.onCreate = function () {
        self.tasks = null;
    };

    self.onStart = function () {
        self.tasks = [];
    };

    self.onPause = function () {
        for (var i = 0; i < self.tasks.length; i++) {
            if ("onPause" in self.tasks[i]) {
                self.tasks[i]["onPause"].call();
            }
        }
    };

    self.add = function (t) {
        self.tasks.push(t);
    };

    self.update = function () {
        // Remove dead tasks and update live ones
        self.tasks = self.tasks.filter(function (t) {
            return !t.isDone();
        });
        for (var i = 0; i < self.tasks.length; i++) {
            self.tasks[i].update();
        }
    };

    self.paint = function (painter) {
        var zIndex;
        for (var i = 0; i < self.tasks.length; i++) {
            if ("paint" in self.tasks[i]) {
                zIndex = 0;
                if ("getZIndex" in self.tasks[i]) {
                    zIndex = self.tasks[i].getZIndex();
                }
                painter.add(self.tasks[i].paint, zIndex);
            }
        }
    };

    self.onCreate();
}

function Manager(width, height, interval) {
    var self = this;

    var STOPPED = 0;
    var STARTED = 1;
    var RESUMED = 2;
    var PAUSED = 3;

    self.onCreate = function (width, height, interval) {
        // Frame interval
        self.interval = interval;

        // Screen info
        self.screens = {};
        self.screensState = {};
        self.screenId = null;
        self.postPaint = null;

        // Create the canvas
        self.canvas = document.getElementById("main");
        self.canvas.width = width;
        self.canvas.height = height;
        self.canvas.style.position = "absolute";
        self.canvas.style.top = 0;
        self.canvas.style.left = 0;
        self.c2 = self.canvas.getContext("2d");

        // Input handling
        // mouse
        self.mouseDown = null;
        self.canvas.onmousedown = function (e) {
            self.mouseDown = {pageX: e.pageX, pageY: e.pageY};
        };
        // keyboard
        self.keyDown = null;
        document.body.onkeydown = function (e) {
            self.keyDown = {key: e.which};
        };
    };

    self.registerScreen = function (screen) {
        console.log(screen.screenId + " " + "onCreate()");
        self.screens[screen.screenId] = screen;

        // Setup subtasks
        screen.subtasks = new Subtasks();

        // Setup painter
        screen.painter = new Painter();

        // Initial state is STOPPED
        // (at this stage onCreate has already been called)
        self.screensState[screen.screenId] = STOPPED;

        // Set the default screen if we need to
        if (!self.screenId) {
            self.screenId = screen.screenId;
        }
    };

    self.getScreen = function () {
        return self.screens[self.screenId];
    };

    self.startScreen = function (screenId) {
        console.log(screenId + " " + "onStart()");
        // Set screen and call onStart
        self.screenId = screenId;
        var screen = self.getScreen();
        if ("onStart" in screen) {
            screen["onStart"].call();
        }
        // Start subtasks
        screen.subtasks.onStart();
        self.screensState[screen.screenId] = STARTED;
    };

    self.resumeScreen = function (screenId) {
        // Clear inputs
        self.mouseDown = {};

        // Call onStart if screen is currently stopped
        self.screenId = screenId;
        var screen = self.getScreen();
        if (self.screensState[screen.screenId] == STOPPED) {
            self.startScreen(screenId);
        }

        // Call onResume
        console.log(screenId + " " + "onResume()");
        if ("onResume" in screen) {
            screen["onResume"].call();
        }
        self.screensState[screen.screenId] = RESUMED;
    };

    self.pauseScreen = function () {
        // Get current screen and call onPause
        var screen = self.getScreen();
        console.log(screen.screenId + " " + "onPause()");
        if ("onPause" in screen) {
            screen["onPause"].call();
        }
        // Pause subtasks
        screen.subtasks.onPause();
        self.screensState[screen.screenId] = PAUSED;

        // Set the current screen to null
        self.screenId = null;
    };

    self.changeScreen = function (screenId) {
        // Get current screen and call onPause and onStop
        self.stopScreen();

        // Set new screen and call onStop if screen is not currently stopped
        self.screenId = screenId;
        var screen = self.getScreen();
        if (self.screensState[screen.screenId] != STOPPED) {
            self.stopScreen();
        }

        // Call onStart and onResume
        self.resumeScreen(screenId);
    };

    self.stopScreen = function () {
        // Call onPause if screen is currently resumed
        var screen = self.getScreen();
        if (self.screensState[screen.screenId] == RESUMED) {
            self.pauseScreen();
        }

        // Call onStop
        console.log(screen.screenId + " " + "onStop()");
        if ("onStop" in screen) {
            screen["onStop"].call();
        }
        self.screensState[screen.screenId] = STOPPED;
    };

    self.start = function () {
        var screen = self.getScreen();
        self.resumeScreen(screen.screenId);
        self.screensState[screen.screenId] = RESUMED;

        window.setInterval(function () {
            self.run();
        }, self.interval);
    };

    self.run = function () {
        self.update();
        self.paint();
    };

    self.update = function () {
        var screen = self.getScreen();
        if (screen) {
            // Handle inputs
            if ("onInput" in screen) {
                screen["onInput"].call();
            }
            screen.update();
            screen.subtasks.update();
        }
    };

    self.paint = function () {
        var screen = self.getScreen();
        if (screen) {
            screen.paint();
            screen.subtasks.paint(screen.painter);
            screen.painter.paint(self.c2);
        }
    };

    self.onCreate(width, height, interval);
}

