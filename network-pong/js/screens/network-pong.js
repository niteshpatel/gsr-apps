const GsrUrl = "http://gamestatereflector.azurewebsites.net/api/state/state";

function NetworkPong(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "network-pong";
        self.parent = parent;

        self.game = getQueryStringParam("game") || "game1";
        self.player1 = getQueryStringParam("player1");
    };

    self.onStart = function () {
        self.bat1Pos = {x: 0, y: 0};
        self.bat2Pos = {x: 400, y: 0};
        self.ballPos = {x: 80, y: 40};
        self.ballSpeed = {x: 8, y: 5};
        self.ballColor = 0;
        self.ballSource = null;

        self.buffer = {};
        self.isUpdatesPending = false;
        self.isRequestPending = false;
    };

    self.onInput = function () {

        if (isInputsPending()) {
            var key = getPressedKey();

            clearPendingInputs();

            if (isUpArrowKey(key)) {
                moveMyBatUp();
            }

            else if (isDownArrowKey(key)) {
                moveMyBatDown();
            }
        }
    };

    self.update = function () {
        if (isPendingUpdatesFromServer()) {
            ApplyPendingUpdatesFromServer();
        }

        if (isBallComingTowardsMe()) {
            moveBall();

            if (isBallOnMyBat()) {
                updateBallAfterBatHit();
            }

            handleBallBouncingOnWalls();
        }

        if (!self.isRequestPending) {
            saveMyGameStateAndRequestRemoteChangesAndWhenDoneStoreAsPendingUpdates();
        }
    };

    function isUpArrowKey(key) {
        return key == 38;
    }

    function isDownArrowKey(key) {
        return key == 40;
    }

    function setMyBatPos(batPos) {
        if (self.player1) {
            self.bat1Pos.x = batPos.x;
            self.bat1Pos.y = batPos.y;
        }
        else {
            self.bat2Pos.x = batPos.x;
            self.bat2Pos.y = batPos.y;
        }
    }

    function moveMyBatUp() {
        var batPos = getMyBatPos();

        batPos.y -= 20;
        if (batPos.y < 0) {
            batPos.y = 0;
        }

        setMyBatPos(batPos);
    }

    function moveMyBatDown() {
        var batPos = getMyBatPos();

        batPos.y += 20;
        if (batPos.y > 340) {
            batPos.y = 340;
        }

        setMyBatPos(batPos);
    }

    function isInputsPending() {
        return self.parent.keyDown;
    }

    function getPressedKey() {
        return self.parent.keyDown.key;
    }

    function clearPendingInputs() {
        self.parent.keyDown = null;
    }

    function isPendingUpdatesFromServer() {
        return self.isUpdatesPending;
    }

    function moveOpponentsBat() {
        if (self.player1) {
            self.bat2Pos.y = self.buffer.bat2Pos.y;
        }
        else {
            self.bat1Pos.y = self.buffer.bat1Pos.y;
        }
    }

    function moveBallToMatchOpponentsBall() {
        self.ballPos = self.buffer.ballPos;
        self.ballSpeed = self.buffer.ballSpeed;
        self.ballSource = "opponent";
    }

    function clearPendingUpdates() {
        self.isUpdatesPending = false;
        self.buffer = {};
    }

    function getMyName() {
        return self.player1 ? "player1" : "player2";
    }

    function getOpponentsName() {
        return self.player1 ? "player2" : "player1";
    }

    function saveMyGameStateAndRequestRemoteChangesAndWhenDoneStoreAsPendingUpdates() {

        self.isRequestPending = true;

        var payload = {
            source: self.game + "-" + getMyName(),
            query: self.game + "-" + getOpponentsName(),
            state: {
                bat1Pos: self.bat1Pos,
                bat2Pos: self.bat2Pos,
                ballPos: self.ballPos,
                ballSpeed: self.ballSpeed,
                ballSource: self.ballSource
            }
        };

        var handler = function (data) {
            if (data) {
                self.buffer.bat1Pos = data.bat1Pos;
                self.buffer.bat2Pos = data.bat2Pos;
                self.buffer.ballPos = data.ballPos;
                self.buffer.ballSpeed = data.ballSpeed;
                self.buffer.ballSource = data.ballSource;

                self.isUpdatesPending = true;
            }

            self.isRequestPending = false;
        };

        PostJsonAndHandleResponse(GsrUrl, payload, handler);
    }

    function ApplyPendingUpdatesFromServer() {
        moveOpponentsBat();

        if (!isBallComingTowardsMe() && self.buffer.ballSource == "myself") {
            moveBallToMatchOpponentsBall();
        }

        clearPendingUpdates();
    }

    function moveBall() {
        self.ballPos.x += self.ballSpeed.x;
        self.ballPos.y += self.ballSpeed.y;
        self.ballSource = "myself";
    }

    function getMyBatPos() {
        var myBatPos = {};

        if (self.player1) {
            myBatPos.x = self.bat1Pos.x;
            myBatPos.y = self.bat1Pos.y;
        }
        else {
            myBatPos.x = self.bat2Pos.x;
            myBatPos.y = self.bat2Pos.y;
        }

        return myBatPos;
    }

    function isBallOnMyBat() {
        var myBatPos = getMyBatPos();

        return self.ballSpeed.x < 0 &&
            self.ballPos.x <= myBatPos.x + 20 &&
            self.ballPos.y >= myBatPos.y - 20 &&
            self.ballPos.y <= myBatPos.y + 80;
    }

    function updateBallAfterBatHit() {
        self.ballSpeed.x = -self.ballSpeed.x;

        // Change Y based on contact point
        self.ballSpeed.y = (self.ballPos.y - self.bat1Pos.y - 30) / 6;
    }

    function handleBallBouncingOnWalls() {
        if (self.ballPos.x < 10 && self.ballSpeed.x < 0) {
            self.ballPos.x = 0;
            self.ballSpeed.x = -self.ballSpeed.x;
        }
        if (self.ballPos.y < 10 && self.ballSpeed.y < 0) {
            self.ballPos.y = 0;
            self.ballSpeed.y = -self.ballSpeed.y;
        }
        if (self.ballPos.x > 380 && self.ballSpeed.x > 0) {
            self.ballPos.x = 380;
            self.ballSpeed.x = -self.ballSpeed.x;
        }
        if (self.ballPos.y > 400 && self.ballSpeed.y > 0) {
            self.ballPos.y = 400;
            self.ballSpeed.y = -self.ballSpeed.y;
        }
    }

    function isPlayer1AndBallGoingLeft() {
        return self.player1 && self.ballSpeed.x < 0;
    }

    function isPlayer2AndBallGoingRight() {
        return !self.player1 && self.ballSpeed.x > 0;
    }

    function isBallComingTowardsMe() {
        return isPlayer1AndBallGoingLeft() || isPlayer2AndBallGoingRight();
    }

    function clearScreen() {
        self.painter.add(function (c) {
            c.clearRect(0, 0, c.canvas.width, c.canvas.height);
        }, 0);
    }

    function drawBorder() {
        self.painter.add(function (c) {
            c.fillStyle = "#000000";
            c.fillRect(0, 0, 420, 420);
        }, 10);
    }

    function paintBat1() {
        self.painter.add(function (c) {
            c.fillStyle = "#0000FF";
            c.fillRect(self.bat1Pos.x, self.bat1Pos.y, 20, 80);
        }, 15);
    }

    function paintBat2() {
        self.painter.add(function (c) {
            c.fillStyle = "#00FF00";
            c.fillRect(self.bat2Pos.x, self.bat2Pos.y, 20, 80);
        }, 15);
    }

    function paintBall() {
        self.painter.add(function (c) {
            self.ballColor += 1;
            self.ballColor = self.ballColor % 22;
            var letters = '56789ABCDEFFEDCBA98765'.split('');
            var r = letters[self.ballColor];
            c.beginPath();
            c.arc(self.ballPos.x + 10, self.ballPos.y + 10, 8, 0, 2 * Math.PI);
            c.fillStyle = "#FF" + r + r + r + r;
            c.fill();
        }, 20);
    }

    self.paint = function () {
        clearScreen();
        drawBorder();
        paintBat1();
        paintBat2();
        paintBall();
    };

    self.onCreate(parent);
}

function getQueryStringParam(sVar) {
    return decodeURI(window.location.search.replace(
        new RegExp("^(?:.*[&\\?]" + decodeURI(sVar)
                .replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function PostJsonAndHandleResponse(url, data, handler) {
    $.ajax(url, {
        data: JSON.stringify(data),
        contentType: 'application/json',
        type: 'POST'

    }).done(handler);
}
